from typing import Iterable, List, Any, Tuple


def add(a: int, b: int) -> int:
    """
    Un essai avec les doctest
    >>> add(2,2)
    4
    """
    return a + b


def get(lst: Tuple[Any, ...], index: int, default=None) -> Any:
    """
    Retourne l'élément de `lst` situé à `index`.

    Si aucun élément ne se trouve à `index`,
    retourne la valeur par défaut.
    """
    try:
        return lst[index]
    except IndexError:
        return default


# Some old stuff
if __name__ == "__main__":
    # Ce code au début permet de lancer des doctests
    import doctest

    doctest.testmod()

    result: float = add(5, 4)
    print(result)
    add(4, 3)

    # S&M unittest
    simple_comme_bonjour = ("pomme", "banane")
    result2: str = get(simple_comme_bonjour, 0, "je laisse la main")
    print(result2)
