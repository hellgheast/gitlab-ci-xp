import src
from src.dummy import add
from src.dummy import get
import pytest


def func(x):
    return x + 1


def raise_excp():
    raise ValueError("XP error")

@pytest.fixture()
def simple_comme_bonjour_fixture():
    return ('pomme','banane')

@pytest.yield_fixture()
def simple_yield_fixture():
    # Anything above yield is the setup
    print('\nBef')
    # In yield is the content of the parameter
    yield  ('pomme','banane')
    # After yield is the teardown
    print('Aft\n')



class TestClass:
    def test_dummy_1(self):
        assert add(3, 3) == 6

    def test_answer(self):
        assert func(4) == 5

    def test_zero_division(self):
        with pytest.raises(ZeroDivisionError):
            1 / 0

    def test_raise_excp(self):
        with pytest.raises(ValueError):
            raise_excp()

    def test_orwell(self):
        assert add(2, 2) == 4

    def test_get(self):
        simple_comme_bonjour = ("pomme", "banane")
        elem = get(simple_comme_bonjour, 0)
        assert elem == "pomme"

    def test_elem_missing(self):
        simple_comme_bonjour = ("pomme", "banane")
        elem = get(simple_comme_bonjour, 1000, "Je laisse ma main")
        assert elem == "Je laisse ma main"

    #def test_with_error(self):
    #    simple_comme_bonjour = ('pomme', 'banane')
    #    simple_comme_bonjour[1000]

    #def test_with_failure(self):
    #    simple_comme_bonjour = ('pomme', 'banane') 
    #    element = get(simple_comme_bonjour, 1000, "Je laisse la main")
    #    assert  element == "My name is Luke"

    def test_element_manquant(self,simple_comme_bonjour_fixture):
        element = get(simple_comme_bonjour_fixture, 1000, 'Je laisse la main')
        assert element == 'Je laisse la main'
    
    def test_compete(self,simple_yield_fixture):
        elem = get(simple_yield_fixture,2,'Auf wiedersehen')
        assert elem == 'Auf wiedersehen'