import unittest
import src
from src.dummy import get

# Cette classe est un groupe de tests. Son nom DOIT commencer
# par 'Test' et la classe DOIT hériter de unittest.TestCase.
class TestFonctionGet(unittest.TestCase):

    def setUp(self):
        self.simple_comme_bonjour = ("pomme", "banane")
    
    def tearDown(self):
        print(" Cleaning!")


    # Chaque méthode dont le nom commence par 'test_'
    # est un test.
    def test_get_element(self):
        element = get(self.simple_comme_bonjour, 0)

        # Le test le plus simple est un test d'égalité. On se
        # sert de la méthode assertEqual pour dire que l'on
        # s'attend à ce que les deux éléments soient égaux. Sinon
        # le test échoue.
        self.assertEqual(element, "pomme")

    def test_element_missing(self):
        element = get(
            self.simple_comme_bonjour, len(self.simple_comme_bonjour) + 2, "Je vois l'avenir"
        )

        # Le test le plus simple est un test d'égalité. On se
        # sert de la méthode assertEqual pour dire que l'on
        # s'attend à ce que les deux éléments soient égaux. Sinon
        # le test échoue.
        self.assertEqual(element, "Je vois l'avenir")

    # Ce code ne peut pas marcher car il n'y a pas 1000
    # éléments dans mon tuple.
    #def test_avec_error(self):
    #    self.simple_comme_bonjour[1000]

    #def test_avec_echec(self):
    #    element = get(self.simple_comme_bonjour, 1000, "Je laisse la main")
        # Ici j'ajoute ARTIFICIELLEMENT une erreur, mais on est bien d'accord
        # que normalement, si ça échoue ici, c'est que votre code ne se comporte
        # pas comme prévu. Personne ne nique ses tests volontairement, sauf
        # les rédacteurs de tutos et les étudiants en histoire de l'art.
    #    self.assertEqual(element, "Je tres clair, Luc")

        # element ne sera pas égal à "Je tres clair, Luc", il sera égal à
        # 'Je laisse la main'. assertEqual va s'en rendre compte et va
        # déclarer que le test a échoué, puisque qu'elle vérifie l'égalité.


# Ceci lance le test si on exécute le script
# directement.
if __name__ == "__main__":
    unittest.main()
